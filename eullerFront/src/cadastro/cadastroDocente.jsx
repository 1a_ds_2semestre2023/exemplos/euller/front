import * as React from "react";
import { useState, useEffect } from "react";
import Alert from "@mui/material/Alert";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import api from "../axios/axios";
import Avatar from "@mui/material/Avatar";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import AccessibilityNewIcon from "@mui/icons-material/AccessibilityNew";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { Navigate } from "react-router-dom";
import Stack from "@mui/material/Stack";
import Snackbar from '@mui/material/Snackbar';

function cadastroDocente({ setNewDocente, newDocente }) {
  const [notification, setNotification] = useState({
    status: false,
    type: "",
    message: "",
  });
  const [docenteDefault, setDocenteDefault] = useState({
    nome: "",
    cargaHorariaTotal: 0,
  });
  const [docente, setDocente] = useState(docenteDefault);

  function onChange(event) {
    const { name, value } = event.target;
    setDocente({ ...docente, [name]: value });
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    createDocente();
  };
  async function createDocente() {
    try {
      const response = await api.postDocente(docente);
      setNewDocente(newDocente + 1);
      setDocente(docenteDefault);
      setNotification({
        status: true,
        type: "success",
        message: response.data.message,
      });
    
    } catch (error) {
      if (error.response) {
        setNotification({
          status: true,
          type: "error",
          message: error.response.data.message,
        });
       
      }
    }
  }
  const defaultTheme = createTheme();

  useEffect(() => {}, []);

  const handleCloseAlert = () => {
    setNotification({
      status: false,
      type: "",
      message: "",
    });
  };

  return (
    <div>
        <ThemeProvider theme={defaultTheme}>
          <Container component="main" maxWidth="xs">
            <Snackbar
              open={notification.status}
              autoHideDuration={3000}
              onClose={handleCloseAlert}
              sx={{ width: "50%" }}
            >
                <Alert severity={notification.type} sx={{ width: "50%" }}>
                  {notification.message}
                </Alert>
            </Snackbar>
            <CssBaseline />
            
              <Box
                sx={{
                  marginTop: 1,
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                }}
              >
                
                <Typography component="h1" variant="h5">
                  Cadastro de Docentes
                </Typography>
                <Box
                  component="form"
                  onSubmit={handleSubmit}
                  noValidate
                  sx={{ mt: 1 }}
                >
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    type="text"
                    id="nome"
                    label="Nome"
                    name="nome"
                    value={docente.nome}
                    onChange={onChange}
                  />
                  <TextField
                    margin="normal"
                    required
                    fullWidth
                    name="cargaHorariaTotal"
                    label="Carga Horaria Total"
                    type="number"
                    id="cargaHorariaTotal"
                    value={docente.cargaHorariaTotal}
                    onChange={onChange}
                  />

                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    sx={{ mt: 3, mb: 2, backgroundColor: "black" }}
                  >
                    Cadastrar
                  </Button>
                </Box>
              </Box>
         
          </Container>
        </ThemeProvider>
    </div>
  );
}
export default cadastroDocente;
