import * as React from "react";
import { useState, useEffect } from "react";
import MenuItem from '@mui/material/MenuItem';
import Select from '@mui/material/Select';
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import api from "../axios/axios";
import Avatar from "@mui/material/Avatar";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { Navigate } from "react-router-dom";

function Cadastrocurso() {
  const [auth, setAuth] = useState("");
  const [curso, setCurso] = useState({
    tipo: "",
    nome: "",
    cargaHoraria: 0,
    sigla: "",
  });

  function onChange(event) {
    const { name, value } = event.target;
    setCurso({ ...curso, [name]: value });
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    createCurso();
  };
  async function createCurso() {
    await api.postCurso(curso).then(
      (response) => {
        alert(response.data.message);
      },
      (error) => {
        console.log("Erro: " + error);
      }
    );
  }

  const defaultTheme = createTheme();

  useEffect(() => {
    setAuth(localStorage.getItem("authenticated"));
  }, []);

  return (
    <div>
      {auth === null ? (
        <Navigate to="/" />
      ) : (
        <ThemeProvider theme={defaultTheme}>
          <Container component="main" maxWidth="xs">
            <CssBaseline />
            <Box
              sx={{
                marginTop: 8,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Avatar sx={{ m: 1, bgcolor: "black" }}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Cadastro de Cursos
              </Typography>
              <Box
                component="form"
                onSubmit={handleSubmit}
                noValidate
                sx={{ mt: 1 }}
              >
                <Select
                  labelId="demo-select-small-label"
                  id="tipo"
                  name="tipo"
                  value={curso.tipo}
                  onChange={onChange}
                  label='Tipo de Curso'
                  fullWidth
                >
                  <MenuItem value={'Fic'}>Fic</MenuItem>
                  <MenuItem value={'Cai'}>Cai</MenuItem>
                  <MenuItem value={'Tc'}>Tc</MenuItem>
                </Select>
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="nome"
                  label="Nome"
                  type="text"
                  id="nome"
                  value={curso.nome}
                  onChange={onChange}
                  autoComplete="current-password"
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  type="text"
                  id="sigla"
                  label="Sigla"
                  name="sigla"
                  value={curso.sigla}
                  onChange={onChange}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="cargaHoraria"
                  label="Carga Horária"
                  type="number"
                  id="cargaHoraria"
                  value={curso.cargaHoraria}
                  onChange={onChange}
                  autoComplete="current-password"
                />
                
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2, backgroundColor: "black" }}
                >
                  Cadastrar
                </Button>
              </Box>
            </Box>
          </Container>
        </ThemeProvider>
      )}
    </div>
  );
}
export default Cadastrocurso;
