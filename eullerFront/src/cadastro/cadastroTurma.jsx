import * as React from "react";
import { useState, useEffect } from "react";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import api from "../axios/axios";
import CssBaseline from "@mui/material/CssBaseline";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { Navigate } from "react-router-dom";
import InputLabel from "@mui/material/InputLabel";

function Cadastroturma({ setNewTurma, newTurma }) {
  const [auth, setAuth] = useState("");
  const [turmaDefault, setTurmaDefault] = useState({
    duracaoAula: 0,
    periodo: "",
    status: true,
    diaSemana:[],
    dateInit:'',
    dateEnd:'',
    curso: {},
    nome:''
  });
  const [turma, setTurma] = useState(turmaDefault);

  function onChange(event) {
    const { name, value } = event.target;
    setTurma({ ...turma, [name]: value });
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    createTurma();
  };
  async function createTurma() {
    await api.postTurma(turma).then(
      (response) => {
        setTurma(turmaDefault);
        setNewTurma(newTurma + 1);
      },
      (error) => {
        console.log("Erro: " + error);
      }
    );
  }

  const defaultTheme = createTheme();
  const [cursos, setCursos] = useState([]);
  async function getCursos() {
    // Aqui devemos criar a chamada de nossa Api
    await api.getCursos().then(
      (response) => {
        console.log(response.data.cursos);
        setCursos(response.data.cursos);
      },
      (error) => {
        console.log("Erro: " + error);
      }
    );
  }

  const listCursos = cursos.map((curso) => {
    return <MenuItem value={curso}>{curso.nome}</MenuItem>;
  });

  useEffect(() => {
    setAuth(localStorage.getItem("authenticated"));
    getCursos();
  }, []);

  return (
    <div>
      {auth === null ? (
        <Navigate to="/" />
      ) : (
        <ThemeProvider theme={defaultTheme}>
          <Container component="main" maxWidth="xs">
            <CssBaseline />
            <Box
              sx={{
                marginTop: 1,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Typography component="h1" variant="h5">
                Cadastro de Turmas
              </Typography>
              <Box
                component="form"
                onSubmit={handleSubmit}
                noValidate
                sx={{ mt: 1 }}
              >
                <InputLabel id="demo-select-small-label">
                  Duração da Aula *
                </InputLabel>
                <Select
                  labelId="demo-select-small-label"
                  id="duracaoAula"
                  name="duracaoAula"
                  value={turma.duracaoAula}
                  onChange={onChange}
                  fullWidth
                >
                  <MenuItem value={45}>45 Minutos</MenuItem>
                  <MenuItem value={50}>50 Minutos</MenuItem>
                  <MenuItem value={60}>60 Minutos</MenuItem>
                </Select>
                <InputLabel id="demo-select-small-label">Período *</InputLabel>
                <Select
                  labelId="demo-select-small-label"
                  id="periodo"
                  name="periodo"
                  value={turma.periodo}
                  onChange={onChange}
                  fullWidth
                  style={{ marginTop: "1%" }}
                >
                  <MenuItem value={"Manhã"}>Manhã</MenuItem>
                  <MenuItem value={"Tarde"}>Tarde</MenuItem>
                  <MenuItem value={"Integral"}>Integral</MenuItem>
                  <MenuItem value={"Noite"}>Noite</MenuItem>
                </Select>
                <InputLabel id="demo-select-small-label">Curso *</InputLabel>
                <Select
                  labelId="demo-select-small-label"
                  id="curso"
                  name="curso"
                  value={turma.curso}
                  onChange={onChange}
                  fullWidth
                  style={{ marginTop: "1%" }}
                >
                  {listCursos}
                </Select>
                <InputLabel id="demo-select-small-label">Data prevista para inicio da turma *</InputLabel>
                <TextField  
                    type="date"
                    id="dateInit"
                    name="dateInit"
                    fullWidth
                    value={turma.dateInit}
                    onChange={onChange}
                    style={{ marginTop: "1%" }}
                    >
                </TextField>
                <InputLabel id="demo-select-small-label">Data prevista para termino da turma *</InputLabel>
                <TextField  
                    type="date"
                    id="dateEnd"
                    name="dateEnd"
                    fullWidth
                    value={turma.dateEnd}
                    onChange={onChange}
                    style={{ marginTop: "1%" }}
                    >
                </TextField>
                <InputLabel id="demo-select-small-label">Dias da Semana</InputLabel>
                <Select
                  labelId="demo-select-small-label"
                  id="diaSemana"
                  multiple
                  name="diaSemana"
                  value={turma.diaSemana}
                  onChange={onChange}
                  fullWidth
                  style={{ marginTop: "1%" }}
                >
                  <MenuItem value={"Seg"}>Seg</MenuItem>
                  <MenuItem value={"Ter"}>Ter</MenuItem>
                  <MenuItem value={"Qua"}>Qua</MenuItem>
                  <MenuItem value={"Qui"}>Qui</MenuItem>
                  <MenuItem value={"Sex"}>Sex</MenuItem>
                  <MenuItem value={"Sab"}>Sab</MenuItem>
                </Select>
                
                <Box
                  sx={{
                    marginTop: 1,
                    display: "flex",
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                  fullWidth
                >
                  
                    <TextField
                      type="text"
                      id="nome"
                      name="nome"
                      fullWidth
                      label="Nome da Turma *"
                      value={turma.nome}
                      onChange={onChange}
                    ></TextField>
                  
                </Box>
                
                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2, backgroundColor: "black" }}
                >
                  Cadastrar
                </Button>
              </Box>
            </Box>
          </Container>
        </ThemeProvider>
      )}
    </div>
  );
}
export default Cadastroturma;
