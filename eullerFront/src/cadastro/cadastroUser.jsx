import * as React from "react";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import TextField from "@mui/material/TextField";
import Box from "@mui/material/Box";
import LockOutlinedIcon from "@mui/icons-material/LockOutlined";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { useState, useEffect } from "react";
import api from "../axios/axios";
import { useNavigate, Navigate } from "react-router-dom";

const defaultTheme = createTheme();

function Cadastro() {
  const [auth, setAuth] = useState("");
  const [user, setUser] = useState({
    sn: "",
    password: "",
    email: "",
  });

  function onChange(event) {
    const { name, value } = event.target;
    setUser({ ...user, [name]: value });
  }
  const handleSubmit = (event) => {
    event.preventDefault();
    createUser();
  };

  async function createUser() {
    try {
      const response = await api.postUser(user);
      alert(response.data.message) 
    } catch (error) {
      if (error.response) {
        alert(error.response.data.message) 
      }
    }
  }

  useEffect(() => {
    setAuth(localStorage.getItem("authenticated"));
  }, []);

  return (
    <div>
      {auth === null ? (
        <Navigate to="/" />
      ) : (
        <ThemeProvider theme={defaultTheme}>
          <Container component="main" maxWidth="xs">
            <CssBaseline />
            <Box
              sx={{
                marginTop: 8,
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Avatar sx={{ m: 1, bgcolor: "black" }}>
                <LockOutlinedIcon />
              </Avatar>
              <Typography component="h1" variant="h5">
                Cadastro
              </Typography>
              <Box
                component="form"
                onSubmit={handleSubmit}
                noValidate
                sx={{ mt: 1 }}
              >
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  type="text"
                  id="sn"
                  label="Sn"
                  name="sn"
                  value={user.sn}
                  onChange={onChange}
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="password"
                  label="Senha"
                  type="password"
                  id="password"
                  value={user.password}
                  onChange={onChange}
                  autoComplete="current-password"
                />
                <TextField
                  margin="normal"
                  required
                  fullWidth
                  name="email"
                  label="Email"
                  type="email"
                  id="email"
                  value={user.email}
                  onChange={onChange}
                />

                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2, backgroundColor: "black" }}
                >
                  Cadastro
                </Button>
              </Box>
            </Box>
          </Container>
        </ThemeProvider>
      )}
    </div>
  );
}
export default Cadastro;
