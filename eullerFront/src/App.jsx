//Import components
import Footer from "./components/rodape/footer";
import Menu from "./components/menu/menu";
import Home from "./home/home";
//Import funcões do router
import { BrowserRouter, Routes, Route, Outlet} from "react-router-dom";

//Import páginas para navegação
import Login from "./login/login";
import Cadastro from "./cadastro/cadastroUser";
import CadastroCurso from "./cadastro/cadastroCurso";
import CadastroTurma from "./cadastro/cadastroTurma";
import CadastroDocente from "./cadastro/cadastroDocente"
import Relatorios from "./relatorios/relatorios";
import AtribuicaoAulas from "./components/atribuicaoAulas/atribuicaoAulas";


function Layout() {
  return (
    <div>
      <Menu />
      <Outlet />
      <Footer />
    </div>
  );
}

function App() {
  
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Layout/>}>
          <Route path="/" element={<Login/>} />
          <Route path="/home" element={<Home />} /> 
          <Route path="/cadastro" element={<Cadastro/>} />
          <Route path="/cadastrocurso" element={<CadastroCurso/>} />
          <Route path="/cadastroturma" element={<CadastroTurma/>} />
          <Route path="/cadastrodocente" element={<CadastroDocente/>} />
          <Route path="/relatorios" element={<Relatorios/>} /> 
          <Route path="/aulas" element={<AtribuicaoAulas/>} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
