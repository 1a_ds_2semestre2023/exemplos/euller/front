import * as React from "react";
import { useState, useEffect } from "react";
import { Button } from "@mui/material";
import { useNavigate, Navigate } from "react-router-dom";
import { styled } from "@mui/material/styles";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import ListCursos from "../list/listCursos";
import ListDocentes from "../list/listDocentes";
import ListTumas from "../list/listTurmas";
import ListUsers from "../list/listUsers";

function Home() {
  const [value, setValue] = React.useState(0);
  const [auth, setAuth] = useState("");
  const navigate = useNavigate();
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  function RedirectCadastrarUser() {
    return navigate("/cadastro");
  }
  function RedirectCadastrarCurso() {
    return navigate("/cadastrocurso");
  }
  function RedirectCadastrarTurma() {
    return navigate("/cadastroturma");
  }
  function RedirectCadastrarDocente() {
    return navigate("/cadastrodocente");
  }

  useEffect(() => {
    setAuth(localStorage.getItem("authenticated"));
  }, []);

  const AntTabs = styled(Tabs)({
    borderBottom: "1px solid #e8e8e8",
    "& .MuiTabs-indicator": {
      backgroundColor: "black",
    },
  });

  function CustomTabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box sx={{ p: 3 }}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }

  CustomTabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
  };

  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
    };
  }

  return (
    <div>
      {auth === null ? (
        <Navigate to="/" />
      ) : (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            flexDirection: "column",
          }}
        >
          <Box
            sx={{
              display: "flex",
              justifyContent: "center",
              flexDirection: "row",
              margin: "1%",
            }}
          >
            <Button
              onClick={RedirectCadastrarCurso}
              style={{ backgroundColor: "black", color: "white", margin: "1%" }}
            >
              Cadastrar Curso
            </Button>
            <Button
              onClick={RedirectCadastrarTurma}
              style={{ backgroundColor: "black", color: "white", margin: "1%" }}
            >
              Cadastrar Turma
            </Button>
            <Button
              onClick={RedirectCadastrarDocente}
              style={{ backgroundColor: "black", color: "white", margin: "1%" }}
            >
              Cadastrar Docente
            </Button>
            <Button
              onClick={RedirectCadastrarUser}
              style={{ backgroundColor: "black", color: "white", margin: "1%" }}
            >
              Cadastrar Administrador
            </Button>
          </Box>
          <Box
            sx={{
              width: "100%",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              flexDirection: "column",
            }}
          >
            <Box
              sx={{
                borderBottom: 1,
                borderColor: "divider",
                backgroundColor: "black",
                color: "white",
              }}
            >
              <AntTabs
                value={value}
                onChange={handleChange}
                aria-label="basic tabs example"
              >
                <Tab sx={{ color: "white" }} label="Cursos" {...a11yProps(0)} />
                <Tab sx={{ color: "white" }} label="Turmas" {...a11yProps(1)} />
                <Tab
                  sx={{ color: "white" }}
                  label="Docentes"
                  {...a11yProps(2)}
                />
                <Tab
                  sx={{ color: "white" }}
                  label="Administradores"
                  {...a11yProps(3)}
                />
              </AntTabs>
            </Box>
            <CustomTabPanel value={value} index={0}>
              <ListCursos />
            </CustomTabPanel>
            <CustomTabPanel value={value} index={1}>
              <ListTumas />
            </CustomTabPanel>
            <CustomTabPanel value={value} index={2}>
              <ListDocentes />
            </CustomTabPanel>
            <CustomTabPanel value={value} index={3}>
              <ListUsers />
            </CustomTabPanel>
          </Box>
        </div>
      )}
    </div>
  );
}

export default Home;
