// Import das funcionalidades React
import { useState, useEffect } from "react";
import { Button } from "@mui/material";
// Import das configs de nossa Api
import api from "../axios/axios";
import Typography from "@mui/material/Typography";
import Table from "@mui/material/Table";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableBody from "@mui/material/TableBody";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Paper from "@mui/material/Paper";
//import UpdateDefault from "../components/updateDefault/updateDefault";
import EditIcon from "@mui/icons-material/Edit";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";

function ListUsers() {
  // Constante criada para receber a lista de usuários de nossa API
  const [users, setUsers] = useState([]);
  const [newUser, setNewUser] = useState(0);
  const [showEdit, setShowEdit] = useState({
    status: false,
    _idItem: "",
  });

  async function getUsers() {
    // Aqui devemos criar a chamada de nossa Api
    await api.getUsers().then(
      (response) => {
        setUsers(response.data.users);
      },
      (error) => {
        console.log("Erro: " + error);
      }
    );
  }

  useEffect(() => {
    // Aqui devemos chamar a function getUsers
    getUsers();
  }, [newUser]);

  async function deleteUser(_id) {
    // Aqui devemos criar a chamada de nossa Api
    await api.deleteUser(_id).then(
      (response) => {
        //Atualizar lista
        setNewUser(newUser + 1);
      },
      (error) => {
        console.log("Erro: " + error);
      }
    );
  }

  async function editUser(_idItem) {
    if (showEdit.status === true && showEdit._idItem === _idItem) {
      setShowEdit({
        status: false,
        _idItem: "",
      });
    } else {
      setShowEdit({
        status: true,
        _idItem: _idItem,
      });
    }
  }

  // Criando a constante que irá exibir nossa lista de usuários
  const listUsers = users.map((user) => {
    return (
      <TableRow key={user._id} style={{ borderStyle: "solid" }}>
        <TableCell align="center">{user.sn}</TableCell>
        <TableCell align="center">{user.email}</TableCell>
        <TableCell align="center" style={{ width: "40%" }}>
          <Button
            onClick={() => {
              deleteUser(user._id);
            }}
          >
            <DeleteOutlineIcon style={{ color: "black" }} />
          </Button>
          <Button
            onClick={() => {
              editUser(user._id);
            }}
          >
            <EditIcon style={{ color: "black" }} />
          </Button>
          {/*showEdit.status === true && showEdit._idItem === user._id ? (
            <UpdateDefault ObjectDefault={user} functDefault={"updateUser"} setUpdateDefaultList={setNewUser} updateDefaultList={newUser} />
          ) : (
            <div></div>
          )*/}
        </TableCell>
      </TableRow>
    );
  });

  return (
    <div>
      <TableContainer
        component={Paper}
        style={{
          margin: "2px",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Typography component="h1" variant="h5">
          Lista de usuários
        </Typography>
        <Table size="small" aria-label="a dense table">
          <TableHead style={{ backgroundColor: "black", borderStyle: "solid" }}>
            <TableRow>
              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              >
                Sn
              </TableCell>

              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              >
                Email
              </TableCell>
              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              ></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>{listUsers}</TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}

export default ListUsers;
