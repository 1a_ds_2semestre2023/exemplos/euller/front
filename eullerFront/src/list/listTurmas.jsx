// Import das funcionalidades React
import { useState, useEffect } from "react";
import { Button } from "@mui/material";
// Import das configs de nossa Api
import api from "../axios/axios";
import Typography from "@mui/material/Typography";
import Table from "@mui/material/Table";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableBody from "@mui/material/TableBody";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Paper from "@mui/material/Paper";
import UpdateTurma from '../components/updateTurma/updateTurma'
import EditIcon from "@mui/icons-material/Edit";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";

function ListTumas() {
  // Constante criada para receber a lista de usuários de nossa API
  const [turmas, setTurmas] = useState([]);
  const [newTurma, setNewTurma] = useState(0);

  async function getTurmas() {
    // Aqui devemos criar a chamada de nossa Api
    await api.getTurmas().then(
      (response) => {
        setTurmas(response.data.turmas);
      },
      (error) => {
        console.log("Erro: " + error);
      }
    );
  }

  useEffect(() => {
    // Aqui devemos chamar a function getTurmas
    getCursos();
    getTurmas();
  }, [newTurma]);

  async function deleteTurma(_id) {
    // Aqui devemos criar a chamada de nossa Api
    await api.deleteTurma(_id).then(
      (response) => {
        //Atualizar lista
        setNewTurma(newTurma + 1);
      },
      (error) => {
        console.log("Erro: " + error);
      }
    );
  }
  const [showEdit, setShowEdit] = useState({
    status: false,
    _idItem: "",
  });
  async function editTurma(_idItem) {
    if (showEdit.status === true && showEdit._idItem === _idItem) {
      setShowEdit({
        status: false,
        _idItem: "",
      });
    } else {
      setShowEdit({
        status: true,
        _idItem: _idItem,
      });
    }
  }

  const [cursos, setCursos] = useState([]);
  async function getCursos() {
    // Aqui devemos criar a chamada de nossa Api
    await api.getCursos().then(
      (response) => {
        setCursos(response.data.cursos);
      },
      (error) => {
        console.log("Erro: " + error);
      }
    );
  }

  // Criando a constante que irá exibir nossa lista de turmas
  const listTurmas = turmas.map((turma) => {
    const cursoNome = cursos.map((curso) => {
      if(curso._id===turma.curso){
        return <p>{curso.nome}</p>
      }
    });
    const dataI = new Date(turma.dateInit)
    const dataE = new Date(turma.dateEnd)
    let dateInit = ((dataI.getDate() + 1)) + "/" + ((dataI.getMonth() + 1)) + "/" + dataI.getFullYear(); 
    let dateEnd = ((dataE.getDate() +1 )) + "/" + ((dataE.getMonth() + 1)) + "/" + dataE.getFullYear(); 
    return (
      <TableRow key={turma._id} style={{ borderStyle: "solid", width: "50%" }}>
        <TableCell align="center">{cursoNome}</TableCell>
        <TableCell align="center">{turma.periodo}</TableCell>
        <TableCell align="center">{turma.duracaoAula}</TableCell>
        <TableCell align="center">
          {turma.status === true ? <p>Ativa</p> : <p>Encerrada</p>}
        </TableCell>
        <TableCell align="center"><p>{dateInit}</p><TableRow><p>{dateEnd}</p></TableRow></TableCell>
        <TableCell align="center" style={{ width: "60%" }}>
          <Button
            onClick={() => {
              deleteTurma(turma._id);
            }}
          >
            <DeleteOutlineIcon style={{ color: "black" }} />
          </Button>
          
          <Button
            onClick={() => {
              editTurma(turma._id);
            }}
          >
            <EditIcon style={{ color: "black" }} />
          </Button>
          {showEdit.status === true && showEdit._idItem === turma._id ? (
            <UpdateTurma ObjectTurma={turma} setNewTurma={setNewTurma} newTurma={newTurma} />
          ) : (
            <div></div>
          )}
        </TableCell>
      </TableRow>
    );
  });

  return (
    <div>
      <TableContainer
        component={Paper}
        style={{
          margin: "1px",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Typography component="h1" variant="h5">
          Lista de turmas
        </Typography>
        <Table size="small" aria-label="a dense table">
          <TableHead style={{ backgroundColor: "black", borderStyle: "solid" }}>
            <TableRow>
              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              >
                Nome do Curso
              </TableCell>

              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              >
                Periodo
              </TableCell>
              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              >
                Duração da Aula
              </TableCell>
              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              >
                Status
              </TableCell>
              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              >
                Previsão
              </TableCell>
              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              ></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>{listTurmas}</TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}

export default ListTumas;
