// Import das funcionalidades React
import { useState, useEffect } from "react";
import { Button } from "@mui/material";
// Import das configs de nossa Api
import api from "../axios/axios";
import Typography from "@mui/material/Typography";
import Table from "@mui/material/Table";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableBody from "@mui/material/TableBody";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Paper from "@mui/material/Paper";
//import UpdateDefault from "../components/updateDefault/updateDefault";
import EditIcon from "@mui/icons-material/Edit";
import DeleteOutlineIcon from "@mui/icons-material/DeleteOutline";

function ListCursos() {
  // Constante criada para receber a lista de usuários de nossa API
  const [cursos, setCursos] = useState([]);
  const [newCurso, setNewCurso] = useState(0);

  async function getCursos() {
    // Aqui devemos criar a chamada de nossa Api
    await api.getCursos().then(
      (response) => {
        setCursos(response.data.cursos);
      },
      (error) => {
        console.log("Erro: " + error);
      }
    );
  }

  useEffect(() => {
    // Aqui devemos chamar a function getCursos
    getCursos();
  }, [newCurso]);

  async function deleteCurso(_id) {
    // Aqui devemos criar a chamada de nossa Api
    await api.deleteCurso(_id).then(
      (response) => {
        //Atualizar lista
        setNewCurso(newCurso + 1);
      },
      (error) => {
        console.log("Erro: " + error);
      }
    );
  }
  const [showEdit, setShowEdit] = useState({
    status: false,
    _idItem: "",
  });
  async function editCurso(_idItem) {
    if (showEdit.status === true && showEdit._idItem === _idItem) {
      setShowEdit({
        status: false,
        _idItem: "",
      });
    } else {
      setShowEdit({
        status: true,
        _idItem: _idItem,
      });
    }
  }

  // Criando a constante que irá exibir nossa lista de usuários
  const listCurso = cursos.map((curso) => {
    return (
      <TableRow key={curso._id} style={{ borderStyle: "solid" }}>
        <TableCell align="center">{curso.nome}</TableCell>
        <TableCell align="center">{curso.sigla}</TableCell>
        <TableCell align="center">{curso.cargaHoraria}</TableCell>
        <TableCell align="center">{curso.tipo}</TableCell>
        <TableCell align="center" style={{ width: "40%" }}>
          <Button
            onClick={() => {
              deleteCurso(curso._id);
            }}
          >
            <DeleteOutlineIcon style={{ color: "black" }} />
          </Button>
          <Button
            onClick={() => {
              editCurso(curso._id);
            }}
          >
            <EditIcon style={{ color: "black" }} />
          </Button>
          
          {/*showEdit.status === true && showEdit._idItem === curso._id ? (
            <UpdateDefault ObjectDefault={curso} functDefault={"updateCurso"} setUpdateDefaultList={setNewCurso} updateDefaultList={newCurso} />
          ) : (
            <div></div>
          )*/}
          
        </TableCell>
      </TableRow>
    );
  });

  return (
    <div>
      <TableContainer
        component={Paper}
        style={{
          margin: "2px",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Typography component="h1" variant="h5">
          Lista de cursos
        </Typography>
        <Table size="small" aria-label="a dense table">
          <TableHead style={{ backgroundColor: "black", borderStyle: "solid" }}>
            <TableRow>
              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              >
                Nome
              </TableCell>

              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              >
                Sigla
              </TableCell>
              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              >
                Carga Horária
              </TableCell>
              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              >
                Tipo de Curso
              </TableCell>
              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              ></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>{listCurso}</TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}

export default ListCursos;
