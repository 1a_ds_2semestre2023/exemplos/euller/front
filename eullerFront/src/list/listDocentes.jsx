// Import das funcionalidades React
import { useState, useEffect } from "react";
import { Button } from "@mui/material";
// Import das configs de nossa Api
import api from "../axios/axios";
import Typography from "@mui/material/Typography";
import Table from "@mui/material/Table";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableBody from "@mui/material/TableBody";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Paper from "@mui/material/Paper";
//import UpdateDefault from '../components/updateDefault/updateDefault'
import EditIcon from "@mui/icons-material/Edit";
import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline';

function ListDocentes() {
  // Constante criada para receber a lista de usuários de nossa API
  const [docentes, setDocentes] = useState([]);
  const [newDocente, setNewDocente] = useState(0);

  async function getDocentes() {
    // Aqui devemos criar a chamada de nossa Api
    await api.getDocentes().then(
      (response) => {
        setDocentes(response.data.docentes);
      },
      (error) => {
        console.log("Erro: " + error);
      }
    );
  }

  useEffect(() => {
    // Aqui devemos chamar a function getDocentes
    getDocentes();
  }, [newDocente]);

  async function deleteDocente(_id) {
    // Aqui devemos criar a chamada de nossa Api
    await api.deleteDocente(_id).then(
      (response) => {
        //Atualizar lista
        setNewDocente(newDocente+1)
      },
      (error) => {
        console.log("Erro: " + error);
      }
    );
  }
  const [showEdit, setShowEdit] = useState({
    status: false,
    _idItem: "",
  });
  async function editDocente(_idItem) {
    if (showEdit.status === true && showEdit._idItem === _idItem) {
      setShowEdit({
        status: false,
        _idItem: "",
      });
    } else {
      setShowEdit({
        status: true,
        _idItem: _idItem,
      });
    }
  }

  // Criando a constante que irá exibir nossa lista de usuários
  const listDocentes = docentes.map((docente) => {
    const cargaHorariaOcupada = docente.cargaHorariaTotal - docente.cargaHorariaLivre
    return (
      <TableRow key={docente._id} style={{ borderStyle: "solid" }}>
        <TableCell align="center">{docente.nome}</TableCell>
        <TableCell align="center">{docente.cargaHorariaTotal}</TableCell>
        <TableCell align="center">{cargaHorariaOcupada}</TableCell>
        <TableCell align="center" style={{ width: "40%" }}>
          <Button onClick={()=>{deleteDocente(docente._id)}}><DeleteOutlineIcon style={{color:'black'}}/></Button>
          <Button
            onClick={() => {
              editDocente(docente._id);
            }}
          >
            <EditIcon style={{ color: "black" }} />
          </Button>
          {/*showEdit.status === true && showEdit._idItem === docente._id ? (
            <UpdateDefault ObjectDefault={docente} functDefault={'updateDocente'} setUpdateDefaultList={setNewDocente} updateDefaultList={newDocente} />
          ) : (
            <div></div>
          )*/}
        </TableCell>
      </TableRow>
    );
  });

  return (
    <div>
      <TableContainer component={Paper} style={{ margin: "2px",display:'flex', flexDirection:'column',alignItems:'center'}}>
      <Typography component="h1" variant="h5" >Lista de docentes</Typography>
        <Table size="small" aria-label="a dense table">
          <TableHead
            style={{ backgroundColor: "black", borderStyle: "solid" }}
          >
            <TableRow>
              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              >
                Nome
              </TableCell>

              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              >
                Carga Horária Total
              </TableCell>
              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              >
                Carga Horária Atribuida
              </TableCell>
              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              >
                
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>{listDocentes}</TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}

export default ListDocentes;
