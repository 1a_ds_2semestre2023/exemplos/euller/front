// Import Funções react
import * as React from 'react';
import { useState, useEffect } from "react";

// Import Funções mui
import Avatar from '@mui/material/Avatar';
import Button from '@mui/material/Button';
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import LockOutlinedIcon from '@mui/icons-material/LockOutlined';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';

// Import Api
import api from '../axios/axios';

// Import Funções react router
import {useNavigate,Navigate} from "react-router-dom";

// LOGO
import Logo from "../midias/logo.png";

const defaultTheme = createTheme();

function Login() {
  const [user, setUser] = useState({
    sn: "",
    password: "",
  });
  const navigate = useNavigate();
  const [auth, setAuth] = useState('');

  function onChange(event) {
    const { name, value } = event.target;
    setUser({ ...user, [name]: value });
  }
  const handleSubmit = (event) => {
    event.preventDefault();
    getLogin();
  };

  async function getLogin() {
    await api.postSignIn(user).then(
      (response) => {
        alert(response.data.message);
        if(response.data.status===true){
          localStorage.setItem('authenticated',true)
          return navigate("/home")
        }
        
      },
      (error) => {
        console.log("Erro: " + error);
      }
    );
  }

  useEffect(() => {
    setAuth(localStorage.getItem("authenticated"))
    }, []);
    
  
  return (
    <div>
      {auth==='true'
      ?
      <Navigate to="/home"/>
      :
    // LOGIN
    <Container
        sx={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "space-around",
        }}
      >
        <img
          src={Logo}
          style={{ width: "450px", height: "450px", marginTop: "40px", borderRadius:"25px" }}
        />
    <ThemeProvider theme={defaultTheme}>
      <Container
        component="main"
        maxWidth="xs"
        sx={{
          display: "flex",
          flexDirection: "column",
        }}
      >
        <Box
          sx={{
            marginTop: 8,
            display: "flex",
            flexDirection: "column",
            alignItems: "center"
          }}
        >
          <Avatar sx={{ margin: 1, backgroundColor: "black" }}>
            <LockOutlinedIcon />
          </Avatar>

          <Typography component="h1" variant="h5">
            Class Project
          </Typography>

          <Box
            component="form"
            onSubmit={handleSubmit}
            noValidate
            sx={{ mt: 1 }}
          >
            <TextField
              margin="normal"
              required
              fullWidth
              type="text"
              id="sn"
              label="Sn"
              name="sn"
              value={user.sn}
              onChange={onChange}
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Senha"
              type="password"
              id="password"
              value={user.password}
              onChange={onChange}
              autoComplete="current-password"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{
                marginTop: 3,
                marginBottom: 2,
                backgroundColor: "black",
              }}
            >
              Login
            </Button>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
    </Container>
      }
    </div>
  );
}
export default Login;