import axios from "axios"

const api = axios.create({
    baseURL: "http://localhost:5000/class-project-senai-franca/api/v1/",
    header: {
        'accept': 'application/json', },
});

const sheets = {
    postSignIn:(user) => api.post("sign-in/",user),

    postUser:(user)=>api.post("user/",user),
    getUsers:()=>api.get("user/"),
    updateUser:(user)=>api.put("user/",user),
    deleteUser:(_id)=>api.delete("user/"+_id),

    postCurso:(curso)=>api.post("curso/",curso),
    getCursos:()=>api.get("curso/"),
    updateCurso:(curso)=>api.put("curso/",curso),
    deleteCurso:(_id)=>api.delete("curso/"+_id),

    postTurma:(turma)=>api.post("turma/",turma),
    postNovaAtribuicao:(turma)=>api.post("turma/novaAtribuicao/",turma),
    patchRemoverAtribuicao:(turma)=>api.patch("turma/removerAtribuicao/",turma),
    getTurmas:()=>api.get("turma/"),
    updateTurma:(turma)=>api.put("turma/",turma),
    deleteTurma:(_id)=>api.delete("turma/"+_id),

    postDocente:(docente)=>api.post("docente/",docente),
    getDocentes:()=>api.get("docente/"),
    updateDocente:(docente)=>api.put("docente/",docente),
    deleteDocente:(_id)=>api.delete("docente/"+_id),
}

export default sheets;