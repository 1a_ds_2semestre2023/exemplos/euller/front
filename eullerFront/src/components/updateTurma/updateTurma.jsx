import * as React from "react";
import TextField from "@mui/material/TextField";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import InputLabel from "@mui/material/InputLabel";
import { useEffect, useState } from "react";
import api from "../../axios/axios";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";

function UpdateTurma({ ObjectTurma, setNewTurma, newTurma }) {
  const dataI = new Date(ObjectTurma.dateInit).toISOString().slice(0, 10);
  const dataE = new Date(ObjectTurma.dateEnd).toISOString().slice(0, 10);
  const [turma, setTurma] = useState({
    _id: ObjectTurma._id,
    duracaoAula: ObjectTurma.duracaoAula,
    periodo: ObjectTurma.periodo,
    status: ObjectTurma.status,
    diaSemana: ObjectTurma.diaSemana,
    dateInit: dataI,
    dateEnd: dataE,
    curso: ObjectTurma.curso,
  });

  function onChange(event) {
    const { name, value } = event.target;
    setTurma({ ...turma, [name]: value });
  }

  async function defaultUpdate() {
    await api.updateTurma(turma).then(
      (response) => {
        setNewTurma(newTurma + 1);
      },
      (error) => {
        console.log("Erro: " + error);
      }
    );
  }

  const handleSubmit = (event) => {
    event.preventDefault();
    defaultUpdate();
  };
  const [cursos, setCursos] = useState([]);
  async function getCursos() {
    // Aqui devemos criar a chamada de nossa Api
    await api.getCursos().then(
      (response) => {
        console.log(response.data.cursos);
        setCursos(response.data.cursos);
      },
      (error) => {
        console.log("Erro: " + error);
      }
    );
  }

  const listCursos = cursos.map((curso) => {
    return <MenuItem value={curso._id}>{curso.nome}</MenuItem>;
  });

  useEffect(() => {
    getCursos();
  }, []);

  return (
    <Box
      component="form"
      onSubmit={handleSubmit}
      noValidate
      style={{ width: "80%" }}
    >
      <InputLabel id="demo-select-small-label">Duração da Aula *</InputLabel>
      <Select
        labelId="demo-select-small-label"
        id="duracaoAula"
        name="duracaoAula"
        value={turma.duracaoAula}
        onChange={onChange}
        fullWidth
      >
        <MenuItem value={45}>45 Minutos</MenuItem>
        <MenuItem value={50}>50 Minutos</MenuItem>
        <MenuItem value={60}>60 Minutos</MenuItem>
      </Select>
      <InputLabel id="demo-select-small-label">Período *</InputLabel>
      <Select
        labelId="demo-select-small-label"
        id="periodo"
        name="periodo"
        value={turma.periodo}
        onChange={onChange}
        fullWidth
        style={{ marginTop: "1%" }}
      >
        <MenuItem value={"Manhã"}>Manhã</MenuItem>
        <MenuItem value={"Tarde"}>Tarde</MenuItem>
        <MenuItem value={"Integral"}>Integral</MenuItem>
        <MenuItem value={"Noite"}>Noite</MenuItem>
      </Select>
      <InputLabel id="demo-select-small-label">Curso *</InputLabel>
      <Select
        labelId="demo-select-small-label"
        id="curso"
        name="curso"
        value={turma.curso}
        onChange={onChange}
        fullWidth
        style={{ marginTop: "1%" }}
      >
        {listCursos}
      </Select>
      <InputLabel id="demo-select-small-label">
        Data de inicio da turma *
      </InputLabel>
      <TextField
        type="date"
        id="dateInit"
        name="dateInit"
        fullWidth
        value={turma.dateInit}
        onChange={onChange}
        style={{ marginTop: "1%" }}
      ></TextField>
      <InputLabel id="demo-select-small-label">
        Data de termino da turma *
      </InputLabel>
      <TextField
        type="date"
        id="dateEnd"
        name="dateEnd"
        fullWidth
        value={turma.dateEnd}
        onChange={onChange}
        style={{ marginTop: "1%" }}
      ></TextField>
      <InputLabel id="demo-select-small-label">Dias da Semana</InputLabel>
      <Select
        labelId="demo-select-small-label"
        id="diaSemana"
        multiple
        name="diaSemana"
        value={turma.diaSemana}
        onChange={onChange}
        fullWidth
        style={{ marginTop: "1%" }}
      >
        <MenuItem value={"Seg"}>Seg</MenuItem>
        <MenuItem value={"Ter"}>Ter</MenuItem>
        <MenuItem value={"Qua"}>Qua</MenuItem>
        <MenuItem value={"Qui"}>Qui</MenuItem>
        <MenuItem value={"Sex"}>Sex</MenuItem>
        <MenuItem value={"Sab"}>Sab</MenuItem>
      </Select>
      <Button
        type="submit"
        fullWidth
        variant="contained"
        sx={{ mt: 3, mb: 2, backgroundColor: "black" }}
      >
        Atualizar
      </Button>
    </Box>
  );
}
export default UpdateTurma;
