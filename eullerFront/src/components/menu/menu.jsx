import * as React from "react";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import { Link } from "react-router-dom";
import AppBar from "@mui/material/AppBar";
import Button from "@mui/material/Button";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import MenuBookIcon from '@mui/icons-material/MenuBook';


function Menu() {
  const navigate = useNavigate();
  const [auth,setAuth] = useState('');

  function logout(){
    localStorage.clear();
    return navigate("/")
  }

  useEffect(() => {
    setAuth(localStorage.getItem("authenticated"))
  }, [localStorage.getItem("authenticated")]);


  return (
    <AppBar
      position="static"
      color="default"
      elevation={0}
      sx={{ borderBottom: (theme) => `1px solid ${theme.palette.divider}` }}
    >
      <Toolbar sx={{ flexWrap: "wrap" }}>
        <Typography variant="h6" color="inherit" noWrap sx={{ flexGrow: 1 }}>
          <MenuBookIcon sx={{ fontSize: 40 }}/>
        </Typography>

        <Link to={`/relatorios`}>
          <Button variant="outlined" sx={{ my: 1, mx: 1.5 , backgroundColor: "black", color: "white"}}>
            Visualizar relatórios
          </Button>
        </Link>
        {auth==='true'
        ?
        <Button variant="outlined" sx={{ my: 1, mx: 1.5 , backgroundColor: "black", color: "white"}} onClick={logout}>
          Logout
        </Button>
        :
        <div></div>
        }
      </Toolbar>
    </AppBar>
  );
}
export default Menu;
