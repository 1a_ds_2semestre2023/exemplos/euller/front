import * as React from "react";
import { useEffect, useState } from "react";
import api from "../../axios/axios";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Table from "@mui/material/Table";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableBody from "@mui/material/TableBody";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";
import Paper from "@mui/material/Paper";
import Modal from "@mui/material/Modal";
import AtribuicaoAulasForm from "./atribuicaoAulasForm";
import Snackbar from "@mui/material/Snackbar";
import Alert from "@mui/material/Alert";
import Fab from "@mui/material/Fab";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

function atribuicaoAulas() {
  const [count, setCount] = useState(0);
  const [turmas, setTurmas] = useState([]);
  const [modalOpen, setModalOpen] = useState({
    open: false,
    turma: "",
  });

  async function getTurmas() {
    // Aqui devemos criar a chamada de nossa Api
    await api.getTurmas().then(
      (response) => {
        setTurmas(response.data.turmas);
      },
      (error) => {
        console.log("Erro: " + error);
      }
    );
  }

  // Para mostrar o nome do Curso
  const [cursos, setCursos] = useState([]);
  async function getCursos() {
    // Aqui devemos criar a chamada de nossa Api
    await api.getCursos().then(
      (response) => {
        setCursos(response.data.cursos);
      },
      (error) => {
        console.log("Erro: " + error);
      }
    );
  }

  useEffect(() => {
    // Aqui devemos chamar a function getTurmas e getCursos
    getCursos();
    getTurmas();
  }, [count]);

  const handleClose = () => {
    setModalOpen({
      open: false,
      turma: "",
    });
    setCount(count + 1);
  };
  function openModal(turma) {
    setModalOpen({
      open: true,
      turma: turma,
    });
  }

  async function clickRemove(docenteId, turmaId){
    alert(docenteId)
  }

  // Criando a constante que irá exibir nossa lista
  const listTurmas = turmas.map((turma) => {
    const cursoNome = cursos.map((curso) => {
      if (curso._id === turma.curso) {
        return <p>{curso.nome}</p>;
      }
    });
    const docenteNome = turma.docentes.map((docente) => {
      return (
        <Fab
          variant="extended"
          sx={{ marginTop: "2%" }}
          size="small"
          onClick={() => clickRemove(docente.docente._id, turma._id)}
        >
          {docente.docente.nome}
        </Fab>
      );
    });
    return (
      <TableRow key={turma._id} style={{ borderStyle: "solid", width: "50%" }}>
        <TableCell align="center">
          <div onClick={() => openModal(turma)}>{cursoNome}</div>
        </TableCell>
        <TableCell align="center">
          <div onClick={() => openModal(turma)}>{turma.periodo}</div>
        </TableCell>
        <TableCell align="center">
          <div onClick={() => openModal(turma)}>{turma.duracaoAula}</div>
        </TableCell>
        <TableCell align="center">{docenteNome}</TableCell>
        <TableCell align="center">
          <div onClick={() => openModal(turma)}>
            {turma.status === true ? <p>Ativa</p> : <p>Encerrada</p>}
          </div>
        </TableCell>
      </TableRow>
    );
  });

  return (
    <div>
      <TableContainer
        component={Paper}
        style={{
          margin: "1px",
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Typography component="h1" variant="h5">
          Escolha a turma
        </Typography>
        <Table size="small" aria-label="a dense table">
          <TableHead style={{ backgroundColor: "black", borderStyle: "solid" }}>
            <TableRow>
              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              >
                Nome do Curso
              </TableCell>

              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              >
                Periodo
              </TableCell>
              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              >
                Duração da Aula
              </TableCell>
              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              >
                Docentes
              </TableCell>
              <TableCell
                align="center"
                style={{ color: "#fff", fontWeight: "bold" }}
              >
                Status
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>{listTurmas}</TableBody>
        </Table>
      </TableContainer>
      <Modal
        open={modalOpen.open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <AtribuicaoAulasForm
            turmaId={modalOpen.turma._id}
            turmaPeriodo={modalOpen.turma.periodo}
            turmaDiaSemana={modalOpen.turma.diaSemana}
          />
        </Box>
      </Modal>
    </div>
  );
}
export default atribuicaoAulas;
