import * as React from "react";
import TextField from "@mui/material/TextField";
import MenuItem from "@mui/material/MenuItem";
import Select from "@mui/material/Select";
import InputLabel from "@mui/material/InputLabel";
import { useEffect, useState } from "react";
import api from "../../axios/axios";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import FormControl from "@mui/material/FormControl";

function atribuicaoAulasForm({
    turmaId,
    turmaPeriodo,
    turmaDiaSemana,
  }) {
  const [infoDocenteDefault, setInfoDocenteDefault] = useState({
      turmaId: turmaId,
      idDocente: "",
      turno: [], // 1 ou 2 - Primeiro ou Segundo Turno (Manha/Tarde/Noite)
      diaSemana: [], // O dia da semana (Seg,Ter,Qua...)
      siglaMateria: "", // Sigla relacionada a materia ministrada
      ok: false,
    });
  
  const [infoDocente, setInfoDocente] = useState(infoDocenteDefault);
    function onChange(event) {
      const { name, value } = event.target;
      setInfoDocente({ ...infoDocente, [name]: value });
    }
  
    const handleSubmit = (event) => {
      event.preventDefault();
      postNovaTurma();
    };
  
    async function postNovaTurma() {
      try {
        const response = await api.postNovaAtribuicao(infoDocente);
        alert(response.data.message)
        setInfoDocente(infoDocenteDefault);
      } catch (error) {
        if (error.response.status === 403) {
          alert(error.response.data.message)
        } else {
          alert(error.response.data.message)
        }
      }
    }
  const [docentes, setDocentes] = useState([]);
  
    async function getDocentes() {
      // Aqui devemos criar a chamada de nossa Api
      await api.getDocentes().then(
        (response) => {
          setDocentes(response.data.docentes);
        },
        (error) => {
          console.log("Erro: " + error);
        }
      );
    }
  
    useEffect(() => {
      getDocentes();
    }, []);
  
  const listDocentes = docentes.map((docente) => {
      return <MenuItem value={docente._id}>{docente.nome}</MenuItem>;
    });
  
    return (
        <div>
          <Box
            component="form"
            onSubmit={handleSubmit}
            noValidate
            sx={{
              mt: 1,
              display: "flex",
              flexDirection: "column",
              width: "100%",
            }}
          >
            <Box
              sx={{
                marginTop: 1,
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <FormControl fullWidth sx={{ marginRight: 1 }}>
                <InputLabel id="demo-select-small-label">Docente *</InputLabel>
                <Select
                  labelId="demo-select-small-label"
                  id="idDocente"
                  name="idDocente"
                  value={infoDocente.idDocente}
                  label="Docente *"
                  onChange={onChange}
                >
                  {listDocentes}
                </Select>
              </FormControl>
              <FormControl fullWidth>
                <InputLabel id="demo-select-small-label">
                  Dia da Semana *
                </InputLabel>
                <Select
                  labelId="demo-select-small-label"
                  id="diaSemana"
                  onChange={onChange}
                  name="diaSemana"
                  multiple
                  value={infoDocente.diaSemana}
                  label="Dia da Semana *"
                  style={{ marginTop: "1%" }}
                >
                  {turmaDiaSemana.map((dia) => (
                    <MenuItem key={dia} value={dia}>
                      {dia}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>
            <Box
              sx={{
                marginTop: 1,
                display: "flex",
                flexDirection: "row",
                alignItems: "center",
              }}
            >
              <FormControl fullWidth sx={{ marginRight: 1 }}>
                <InputLabel id="demo-select-small-label">Turno *</InputLabel>
                <Select
                  labelId="demo-select-small-label"
                  id="turno"
                  multiple
                  name="turno"
                  value={infoDocente.turno}
                  label="Turno *"
                  onChange={onChange}
                >
                  {turmaPeriodo === "Manhã" ? (
                    [
                      <MenuItem key={1} value={1}>M - 1° Turno</MenuItem>,
                      <MenuItem key={2} value={2}>M - 2° Turno</MenuItem>,
                    ]
                  ) : turmaPeriodo === "Tarde" ? (
                    [
                      <MenuItem key={3} value={3}>T - 1° Turno</MenuItem>,
                      <MenuItem key={4} value={4}>T - 2° Turno</MenuItem>,
                    ]
                  ) : turmaPeriodo === "Integral" ? (
                    [
                      <MenuItem key={1} value={1}>M - 1° Turno</MenuItem>,
                      <MenuItem key={2} value={2}>M - 2° Turno</MenuItem>,
                      <MenuItem key={3} value={3}>T - 1° Turno</MenuItem>,
                      <MenuItem key={4} value={4}>T - 2° Turno</MenuItem>,
                    ]
                  ) : (
                    [
                      <MenuItem key={5} value={5}>N - 1° Turno</MenuItem>,
                      <MenuItem key={6} value={6}>N - 2° Turno</MenuItem>,
                    ]
                  )}
                </Select>
              </FormControl>
              <FormControl fullWidth>
                <TextField
                  labelId="demo-select-small-label"
                  id="siglaMateria"
                  onChange={onChange}
                  name="siglaMateria"
                  value={infoDocente.siglaMateria}
                  label="Sigla da Matéria *"
                  style={{ marginTop: "1%" }}
                ></TextField>
              </FormControl>
            </Box>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 2, backgroundColor: "black" }}
            >
              Atribuir
            </Button>
          </Box>
        </div>
          );
  
  }
  export default atribuicaoAulasForm;